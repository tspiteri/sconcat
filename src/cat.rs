// Copyright © 2017 Trevor Spiteri

// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#[cfg(feature = "fast_fmt")]
use fast_fmt::{Debug as FFDebug, Display as FFDisplay, Fmt, Write};
use std::fmt::{self, Debug, Display};
use std::ops::{Add, AddAssign};

/// Trait for types that can be concatenated.
pub trait CatItem {
    /// Returns the length of the item in bytes.
    fn size_hint(&self) -> usize;
    /// Appends the item to a `String`.
    fn append_to(&self, s: &mut String);
    /// Converts the item to a `String`.
    fn into_string(self, capacity: usize) -> String;
}

impl<'a> CatItem for char {
    #[inline]
    fn size_hint(&self) -> usize {
        self.len_utf8()
    }

    #[inline]
    fn append_to(&self, s: &mut String) {
        s.push(*self);
    }

    #[inline]
    fn into_string(self, capacity: usize) -> String {
        let mut s = String::with_capacity(capacity);
        s.push(self);
        s
    }
}

impl<'a> CatItem for &'a str {
    #[inline]
    fn size_hint(&self) -> usize {
        self.len()
    }

    #[inline]
    fn append_to(&self, s: &mut String) {
        s.push_str(self);
    }

    #[inline]
    fn into_string(self, capacity: usize) -> String {
        let mut s = String::with_capacity(capacity);
        s.push_str(self);
        s
    }
}

impl CatItem for String {
    #[inline]
    fn size_hint(&self) -> usize {
        self.len()
    }

    #[inline]
    fn append_to(&self, s: &mut String) {
        s.push_str(self)
    }

    #[inline]
    fn into_string(mut self, capacity: usize) -> String {
        let len = self.len();
        if capacity > len {
            self.reserve(capacity - len);
        }
        self
    }
}

#[derive(Clone)]
pub struct CatMany<L: CatItem, R: CatItem> {
    lhs: L,
    rhs: R,
}

impl<L: CatItem + Copy, R: CatItem + Copy> Copy for CatMany<L, R> {}

impl<L: CatItem, R: CatItem> CatItem for CatMany<L, R> {
    #[inline]
    fn size_hint(&self) -> usize {
        self.lhs
            .size_hint()
            .checked_add(self.rhs.size_hint())
            .expect("capacity overflow")
    }

    #[inline]
    fn append_to(&self, s: &mut String) {
        self.lhs.append_to(s);
        self.rhs.append_to(s);
    }

    #[inline]
    fn into_string(self, capacity: usize) -> String {
        let mut s = self.lhs.into_string(capacity);
        self.rhs.append_to(&mut s);
        s
    }
}

impl<L: CatItem, R: CatItem> Add<Cat> for CatMany<L, R> {
    type Output = CatMany<L, R>;
    #[inline]
    fn add(self, _rhs: Cat) -> CatMany<L, R> {
        self
    }
}

impl<L: CatItem, R: CatItem, RR: CatItem> Add<CatOne<RR>> for CatMany<L, R> {
    type Output = CatMany<CatMany<L, R>, RR>;
    #[inline]
    fn add(self, rhs: CatOne<RR>) -> CatMany<CatMany<L, R>, RR> {
        CatMany {
            lhs: self,
            rhs: rhs.inner,
        }
    }
}

impl<L: CatItem, R: CatItem, RR: CatItem> Add<RR> for CatMany<L, R> {
    type Output = CatMany<CatMany<L, R>, RR>;
    #[inline]
    fn add(self, rhs: RR) -> CatMany<CatMany<L, R>, RR> {
        CatMany {
            lhs: self,
            rhs: rhs,
        }
    }
}

impl<L: CatItem, R: CatItem> AddAssign<CatMany<L, R>> for String {
    #[inline]
    fn add_assign(&mut self, rhs: CatMany<L, R>) {
        self.reserve(rhs.size_hint());
        rhs.append_to(self);
    }
}

impl<'a, L: CatItem, R: CatItem> AddAssign<&'a CatMany<L, R>> for String {
    #[inline]
    fn add_assign(&mut self, rhs: &CatMany<L, R>) {
        self.reserve(rhs.size_hint());
        rhs.append_to(self);
    }
}

impl<L: CatItem, R: CatItem> From<CatMany<L, R>> for String {
    #[inline]
    fn from(src: CatMany<L, R>) -> String {
        let capacity = src.size_hint();
        src.into_string(capacity)
    }
}

impl<L: CatItem + Debug, R: CatItem + Debug> Debug for CatMany<L, R> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(&self.lhs, f)?;
        Display::fmt(" + ", f)?;
        Debug::fmt(&self.rhs, f)
    }
}

impl<L: CatItem + Display, R: CatItem + Display> Display for CatMany<L, R> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.lhs, f)?;
        Display::fmt(&self.rhs, f)
    }
}

#[derive(Clone)]
pub struct CatOne<T: CatItem> {
    inner: T,
}

impl<T: CatItem + Copy> Copy for CatOne<T> {}

impl<T: CatItem> Add<Cat> for CatOne<T> {
    type Output = CatOne<T>;
    #[inline]
    fn add(self, _rhs: Cat) -> CatOne<T> {
        self
    }
}

impl<L: CatItem, R: CatItem> Add<CatOne<R>> for CatOne<L> {
    type Output = CatMany<L, R>;
    #[inline]
    fn add(self, rhs: CatOne<R>) -> CatMany<L, R> {
        CatMany {
            lhs: self.inner,
            rhs: rhs.inner,
        }
    }
}

impl<L: CatItem, R: CatItem> Add<R> for CatOne<L> {
    type Output = CatMany<L, R>;
    #[inline]
    fn add(self, rhs: R) -> CatMany<L, R> {
        CatMany {
            lhs: self.inner,
            rhs: rhs,
        }
    }
}

impl<T: CatItem> AddAssign<CatOne<T>> for String {
    #[inline]
    fn add_assign(&mut self, rhs: CatOne<T>) {
        self.reserve(rhs.inner.size_hint());
        rhs.inner.append_to(self);
    }
}

impl<'a, T: CatItem> AddAssign<&'a CatOne<T>> for String {
    #[inline]
    fn add_assign(&mut self, rhs: &CatOne<T>) {
        self.reserve(rhs.inner.size_hint());
        rhs.inner.append_to(self);
    }
}

impl<T: CatItem> From<CatOne<T>> for String {
    #[inline]
    fn from(src: CatOne<T>) -> String {
        let capacity = src.inner.size_hint();
        src.inner.into_string(capacity)
    }
}

impl<T: CatItem + Debug> Debug for CatOne<T> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(&self.inner, f)
    }
}

impl<T: CatItem + Display> Display for CatOne<T> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.inner, f)
    }
}

/// A term that is used to start a string concatenation.
///
/// See the [crate documentation](index.html).
///
/// # Examples
///
/// ```rust
/// use sconcat::Cat;
///
/// let cat = Cat + "Hello, " + "world! " + '☺';
/// let s = String::from(cat);
/// assert_eq!(s, "Hello, world! ☺");
///
/// let mut s2 = String::from("Hello");
/// s2 += Cat + ',' + " world" + String::from("! ") + '☺';
/// assert_eq!(s2, "Hello, world! ☺");
/// ```
#[derive(Clone, Copy)]
pub struct Cat;

impl Add<Cat> for Cat {
    type Output = Cat;
    #[inline]
    fn add(self, _rhs: Cat) -> Cat {
        self
    }
}

impl<T: CatItem> Add<CatOne<T>> for Cat {
    type Output = CatOne<T>;
    #[inline]
    fn add(self, rhs: CatOne<T>) -> CatOne<T> {
        rhs
    }
}

impl<T: CatItem> Add<T> for Cat {
    type Output = CatOne<T>;
    #[inline]
    fn add(self, rhs: T) -> CatOne<T> {
        CatOne { inner: rhs }
    }
}

impl AddAssign<Cat> for String {
    #[inline]
    fn add_assign(&mut self, _rhs: Cat) {}
}

impl<'a> AddAssign<&'a Cat> for String {
    #[inline]
    fn add_assign(&mut self, _rhs: &Cat) {}
}

impl From<Cat> for String {
    #[inline]
    fn from(_src: Cat) -> String {
        String::new()
    }
}

impl Debug for Cat {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt("\"\"", f)
    }
}

impl Display for Cat {
    #[inline]
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        Ok(())
    }
}

// fast_fmt impls here

#[cfg(feature = "fast_fmt")]
impl<L: CatItem + Fmt, R: CatItem + Fmt> Fmt for CatMany<L, R> {
    fn fmt<W: Write>(
        &self,
        writer: &mut W,
        strategy: &FFDisplay,
    ) -> Result<(), W::Error> {
        self.lhs.fmt(writer, strategy)?;
        self.rhs.fmt(writer, strategy)
    }

    #[inline]
    fn size_hint(&self, strategy: &FFDisplay) -> usize {
        Fmt::size_hint(&self.lhs, strategy) +
            Fmt::size_hint(&self.rhs, strategy)
    }
}

#[cfg(feature = "fast_fmt")]
impl<T: CatItem + Fmt> Fmt for CatOne<T> {
    #[inline]
    fn fmt<W: Write>(
        &self,
        writer: &mut W,
        strategy: &FFDisplay,
    ) -> Result<(), W::Error> {
        self.inner.fmt(writer, strategy)
    }

    #[inline]
    fn size_hint(&self, strategy: &FFDisplay) -> usize {
        Fmt::size_hint(&self.inner, strategy)
    }
}

#[cfg(feature = "fast_fmt")]
impl Fmt for Cat {
    // Prints nothing
    #[inline]
    fn fmt<W: Write>(
        &self,
        _writer: &mut W,
        _strategy: &FFDisplay,
    ) -> Result<(), W::Error> {
        Ok(())
    }

    #[inline]
    fn size_hint(&self, _strategy: &FFDisplay) -> usize {
        0
    }
}

#[cfg(feature = "fast_fmt")]
impl<L: CatItem + Fmt<FFDebug>, R: CatItem + Fmt<FFDebug>> Fmt<FFDebug>
    for CatMany<L, R> {
    fn fmt<W: Write>(
        &self,
        writer: &mut W,
        strategy: &FFDebug,
    ) -> Result<(), W::Error> {
        self.lhs.fmt(writer, strategy)?;
        Fmt::fmt(" + ", writer, &FFDisplay)?;
        self.rhs.fmt(writer, strategy)
    }

    #[inline]
    fn size_hint(&self, strategy: &FFDebug) -> usize {
        Fmt::<FFDebug>::size_hint(&self.lhs, strategy) + 3 +
            Fmt::<FFDebug>::size_hint(&self.rhs, strategy)
    }
}

#[cfg(feature = "fast_fmt")]
impl<T: CatItem + Fmt<FFDebug>> Fmt<FFDebug> for CatOne<T> {
    #[inline]
    fn fmt<W: Write>(
        &self,
        writer: &mut W,
        strategy: &FFDebug,
    ) -> Result<(), W::Error> {
        self.inner.fmt(writer, strategy)
    }

    #[inline]
    fn size_hint(&self, strategy: &FFDebug) -> usize {
        Fmt::<FFDebug>::size_hint(&self.inner, strategy)
    }
}

#[cfg(feature = "fast_fmt")]
impl Fmt<FFDebug> for Cat {
    // Prints nothing
    #[inline]
    fn fmt<W: Write>(
        &self,
        writer: &mut W,
        _strategy: &FFDebug,
    ) -> Result<(), W::Error> {
        Fmt::fmt("\"\"", writer, &FFDisplay)
    }

    #[inline]
    fn size_hint(&self, _strategy: &FFDebug) -> usize {
        2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let cat = Cat + "Hello, " + String::from("world");
        assert_eq!(cat.to_string(), "Hello, world");
        assert_eq!(String::from(cat), "Hello, world");

        let mut s = String::new();
        s.reserve(20);
        let ptr = s.as_ptr();
        s += Cat + "12345" + "67890" + '1' + String::from("2345") + "67890";
        assert_eq!(s, "12345678901234567890");
        assert_eq!(s.as_ptr(), ptr);
    }

    #[test]
    fn formatting() {
        let cat0 = Cat;
        assert_eq!(format!("{}", cat0), "");
        assert_eq!(format!("{:?}", cat0), "\"\"");
        let cat1 = cat0 + "Hello, ";
        assert_eq!(format!("{}", cat1), "Hello, ");
        assert_eq!(format!("{:?}", cat1), "\"Hello, \"");
        let cat2 = cat1 + "world! ";
        assert_eq!(format!("{}", cat2), "Hello, world! ");
        assert_eq!(format!("{:?}", cat2), "\"Hello, \" + \"world! \"");
        let cat3 = cat2 + '☺';
        assert_eq!(format!("{}", cat3), "Hello, world! ☺");
        assert_eq!(format!("{:?}", cat3), "\"Hello, \" + \"world! \" + '☺'");
    }

    #[test]
    #[cfg(feature = "fast_fmt")]
    fn fast_formatting() {
        let cat0 = Cat;
        assert_eq!(fast_fmt_display(cat0), "");
        let cat1 = cat0 + "Hello, ";
        assert_eq!(fast_fmt_display(cat1), "Hello, ");
        let cat2 = cat1 + "world! ";
        assert_eq!(fast_fmt_display(cat2), "Hello, world! ");
        let cat3 = cat2 + '☺';
        assert_eq!(fast_fmt_display(cat3), "Hello, world! ☺");
    }

    #[cfg(feature = "fast_fmt")]
    fn fast_fmt_display<T: Fmt<FFDisplay>>(item: T) -> String {
        let mut buf = String::new();
        item.fmt(&mut buf, &FFDisplay).unwrap();
        buf
    }
}
